const gulp = require('gulp'),
	concat = require('gulp-concat'),
	sass = require('gulp-sass'),
	cssnano = require("cssnano"),
	postcss = require("gulp-postcss"),
	autoprefixer = require('autoprefixer'),
	size = require('gulp-size'),
	rename = require('gulp-rename'),
	del = require('del'),
	mediaQueriesSplitter = require('gulp-media-queries-splitter'),
	image = require('gulp-image'),
	gcmq = require('gulp-group-css-media-queries'),
	newer = require('gulp-newer'),
	beeper = require('beeper');

var dest = 'app/';
var sassSources = [
	dest+'scss/styles.scss'
];
var resetSources = [
	dest+'scss/critical.scss'
];


// Clean CSS folder

function clean() {
	return del([dest + 'css']);
}


// CSS tasks

function compressImages() {
	return gulp
		.src(dest + 'img/**/*')
		.pipe(newer(dest + 'images'))
		.pipe(image({
			svgo: false,
			quiet: false
		}))
		.pipe(gulp.dest(dest + 'images'))
}

function compileSass() {
	return gulp
		.src(sassSources)
		.pipe(sass({ outputStyle: "expanded" }))
		.pipe(gcmq())
		.pipe(gulp.dest(dest + 'css'))
}

function compileCriticalSass() {
	return gulp
		.src(resetSources)
		.pipe(sass({ outputStyle: "expanded" }))
		.pipe(gcmq())
		.pipe(gulp.dest(dest + 'css'))
}

function splitCss() {
	return gulp
		.src(dest + 'css/styles.css')
		.pipe(mediaQueriesSplitter([
			//{media: 'all', filename: 'all.css'},
			{media: ['none', {max: '641px'}], filename: 'base.css'},
			{media: {min: '640px', minUntil: '769px'}, filename: 'tablet.css'},
			{media: {min: '1024px'}, filename: 'desktop.css'},
		]))
		.pipe(size({
			showFiles: true
		}))
		.pipe(size({
			showFiles: true,
			gzip: true
		}))
		.pipe(gulp.dest(dest + 'css/dist'))
}

function minifyCriticalCss() {
	return gulp
		.src(dest + 'css/critical.css')
		.pipe(postcss([autoprefixer(), cssnano()]))
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest(dest + 'css/dist'))
		.pipe(size({
			showFiles: true
		}))
		.pipe(size({
			showFiles: true,
			gzip: true
		}))
}
function minifyCss() {
	return gulp
		.src(dest + 'css/dist/*.css')
		.pipe(postcss([autoprefixer(), cssnano()]))
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest(dest + 'css/dist'));
}

function compileScripts() {
	return gulp.src([
		dest + 'libs/jquery/jquery.min.js',
		dest + 'libs/aos/dist/aos.js',
		dest + 'libs/swiper/swiper.min.js',
		dest + 'js/common.js', // Always at the end
	])
		.pipe(concat('scripts.min.js'))
		.pipe(gulp.dest(dest + '/js'));
}

function beep() {
	return beeper();
}

function watch() {
	return (function(){
		gulp.watch(dest+'scss/**/*.scss', gulp.series(['buildStyles', beep]));
		gulp.watch(dest+'js/common.js', gulp.series(['scripts', beep]));
	})();
}


// Define complex tasks

const images = gulp.series(compressImages);
const css = gulp.series(compileSass, compileCriticalSass, splitCss, minifyCss, minifyCriticalCss);
const scripts = gulp.series(compileScripts);
const buildStyles = gulp.series(clean, gulp.parallel(css));


// Export tasks

exports.css = css;
exports.images = images;
exports.clean = clean;
exports.buildStyles = buildStyles;
exports.scripts = scripts;
exports.default = buildStyles;
exports.watch = watch;